function switchTabs() {
  let tabs = document.querySelector(".tabs").children;
  let articles = document.querySelector(".tabs-content").children;

  for (let i = 0; i < tabs.length; i++) {
    tabs[i].addEventListener("click", (event) => {
      let activeTab = document.querySelector(".tabs-title.active");
      activeTab.classList.remove("active");
      event.target.classList.add("active");

      let activeData = document.querySelector("[data-active]");
      activeData.removeAttribute("data-active");
      let contents = document.querySelectorAll(".tabs-content > li");
      contents[i].setAttribute("data-active", "");
    });
  }
}

switchTabs()